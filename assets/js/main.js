(function ($) {
    "use strict";

/*------------------------------------
    Isotope Installer
------------------------------------*/
$(window).on('load', function () {
    
    $('.portfolio_grid').isotope({
        itemSelector: '.grid-item',
        percentPosition: true,
        masonry: {
            columnWidth: '.grid-item'
        }
    });
    $('.filter li').on('click', function () {
        var filterValue = $(this).attr('data-filter');
        $('.portfolio_grid').isotope({
            filter: $(this).attr('data-filter')
        });
    });
});
/*--------------------------------------------
    Client Carosel Installer
--------------------------------------------*/
    $(document).ready(function () {

        $('.carusel_inner_area').owlCarousel({
            loop: true,
            margin: 20,
            nav: false,
            autoplay: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        })
    });
/*--------------------------------------------
    Wow js Settings
--------------------------------------------*/  
 new WOW().init();

/*--------------------------------------------
    Scroll top/bottom jQuery js
--------------------------------------------*/   
var dheight = $(document).height();
var wheight = $(window).height();
var abelableh = (dheight/4*3 - wheight);
//$(".scrolltop").fadeOut();
$(document).on('scroll', function(){
    if(wheight>$(this).scrollTop()){
    $(".scrolltop").fadeIn(500);
}else{
   $(".scrolltop").fadeOut(500); 
}
});
$(".scrolldown").fadeOut();
$(document).on('scroll', function(){
    if($(this).scrollTop()>wheight){
    $(".scrolldown").fadeIn(500);
}else{
   $(".scrolldown").fadeOut(500); 
}
});
/*--------------------------------------------
    navbar sticky jQuery js
--------------------------------------------*/
    $(document).on('scroll', function(){
  if($(this).scrollTop()>58){
      $(".nav-stick").addClass("nav-barstyle");
  }else if($(this).scrollTop()>0){
      $(".nav-stick").removeClass("nav-barstyle");
  }
}); 
/*--------------------------------------------
    Scroll top/bottom jQuery js
--------------------------------------------*/ 
$("#typed").typed({
    stringsElement: $('#typed-strings'),
    typeSpeed: 50,
    backSpeed: 50,
    backDelay: 500,
    startDelay: 1000,
    loop: true,
});
 $('[data-smooth-scroll]').on('click', function (A) {
        var B = $(this).attr('data-animate-time'),
            C = this.hash;
        !B && 0 < C.length && (A.preventDefault(), $('html, body').animate({
            scrollTop: $(C).offset().top
        }, 500, function () {
            window.location.hash = C
        })), 0 < C.length && (A.preventDefault(), $('html, body').animate({
            scrollTop: $(C).offset().top
        }, B, function () {
            window.location.hash = C
        }))
    });
})(jQuery);